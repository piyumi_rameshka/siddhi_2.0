package com.wso2;


import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;


public class SiddhiConfiguration {

    private String appString;
    private String siddhiHomePath;
    private String appName;
    private Logger logger = Logger.getLogger(SiddhiConfiguration.class);


    public static void main(String[] args) {
        //start worker demons


        SiddhiConfiguration siddhiConfiguration = new SiddhiConfiguration();

        siddhiConfiguration.createSiddhiApp(args);
        // siddhiConfiguration.startSidhiWorker();


    }

    public void createSiddhiApp(String[] args) {


        this.siddhiHomePath = args[0];
        this.appName = args[1];
        this.appString = args[2];

        for (int i = 3; i < args.length - 1; i++) {
            appString = appString + " " + args[i];
        }


       // String siddhiFilePath = siddhiHomePath + File.separator + "deployment" + File.separator + "siddhi-files" + File.separator + appName + ".siddhi";
        String siddhiFilePath = siddhiHomePath + File.separator+ appName + ".siddhi";


        BufferedWriter bw = null;
        try {

            File file = new File(siddhiFilePath);

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(appString);


        } catch (IOException e) {

            logger.info(e.getMessage());

        } finally {

            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    public void startSidhiWorker() {


        String siddhiRun = String.format("%s%sbin%seditor.sh", siddhiHomePath, File.separator, File.separator);
        ProcessBuilder siddhiProcess = new ProcessBuilder(siddhiRun);

        try {

            Process process = siddhiProcess.start();


            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));


            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(siddhiHomePath + File.separator + "log.txt")));
            String line;
            while ((line = br.readLine()) != null) {
                bw.write(line);
            }

        } catch (IOException e) {

            logger.info(e.getMessage());
            System.out.println("logging from siddhiConfiguration File");
        }


    }
}
